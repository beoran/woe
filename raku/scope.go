package raku

type Scope interface {
    /* Chains a child scope to a parent scope. */
    Chain(*Scope) bool
    Classifier
}


type DefaultScope struct {
    DefaultClassifier
    Parent Scope
}


func (scope * DefaultScope) Chain(parent Scope) bool {
    scope.Parent = parent
    return true
}

func (scope * DefaultScope) Classify(text TokenText) (TokenType, bool) {
    typ, ok := scope.DefaultClassifier.Classify(text)
    if (!ok && scope.Parent != nil) {
        typ, ok = scope.Parent.Classify(text)
    }
    return typ, ok
}




