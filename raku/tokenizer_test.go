package raku

import (
    // "strings"
    "testing"
    "os"

    "gitlab.com/beoran/woe/monolog"
    // "gitlab.com/beoran/woe/tree"
)

func HelperTryTokenizing(input string, test *testing.T) {
    test.Logf("Tokenizing started:")
    output := Tokenize(input)
    for _, token := range output {
        test.Logf("Token %s", token.String())
    }
}


func TestTokenizing1(test *testing.T) {
    const input = `
say "hello \"world\""
        
define open a door do
    set (door's open) true
    let door 's open be true 
end

def increment variable by value do \
    ( variable = ( variable + value ) )
end

: foo bar, baz, quuux {
  print foo, bar, bqz, quux
}     

`
    HelperTryTokenizing(input, test)
    test.Log("Hi test!")
} 

func TestTokenizing2(test *testing.T) {
    const input = `say`
    HelperTryTokenizing(input, test)
    test.Log("Hi test!")
}

func TestTokenizing3(test *testing.T) {
    const input = `$sym`
    HelperTryTokenizing(input, test)
    test.Log("Hi test!")
}


func TestMain(m *testing.M) { 
    monolog.Setup("raku_test.log", true, false)
    // monolog.EnableLevel("DEBUG")
    retCode := m.Run()
    monolog.Close()
    os.Exit(retCode)
}
