package raku

type Value interface {
}

type StringValue string

type SymbolValue string

type IntegerValue int64

type FloatValue float64

type ArrayValue []Value

type MapValue map[string]Value

type BoolValue bool

/** An instance of a class. */
type Instance interface {
    Send Method
}

/** A class. */
type Class interface {
    Instance
    
}


type Method func (raku * Raku, message string, target Object, args ... interface{}) (Object, error)

type Command struct {
    Name string
    Method 
}

type CommandMap = map[string] Command

type Class interface {
    Object
    Parent() * Class
    Selector(name string) * Method
}

type Object interface {
    Class() Class
    Send(raku * Raku, message string, args ... interface {}) (Object, error)
}

var RootClass * DefaultClass = &DefaultClass{}

type DefaultObject struct {
    class Class
    * Raku
    CommandMap
}

func (object DefaultObject) Selector(name string) Method {
    command, ok := object.CommandMap[name]
    if !ok {
        method := object.class.Selector(name)
        if method == nil { 
            return nil
        }
        return method
    }
    return command.Method
}


func (object DefaultObject) Class() Object {
    return RootClass
}

const METHOD_MISSING = "method_missing"

func (object DefaultObject) Send(raku * Raku, message string, args ... interface {}) (Object, error) {    
    method := object.Selector(message)
    if (method != nil) {
        return method(object.Raku, message, object, args ...)
    }
    method = object.Selector(METHOD_MISSING)
    if (method != nil) {
        return method(object.Raku, message, object, args ...)
    }
    return nil, fmt.Errorf("Cannot send message %s to object %v.", message, object)
}

type DefaultClass struct {
    parent * Class
    DefaultObject
}

func (class DefaultClass) Parent() Object {
    return class.parent
}

type NilClass struct {
    DefaultClass
}

var Nil Object = &DefaultObject{NilClass};

var Root * Object = &DefaultObject{RootClass}

type Boolean struct {
    DefaultObject
}

var True  * Boolean = &Boolean{Root}
var False * Boolean = &Boolean{Root}


func (boolean Boolean) Parent() Object {
    return Nil
}

func (boolean Boolean) Send(raku * Raku, message string, args ... interface {}) (Object, error) {
    return Root.Send(raku, message, args ...);
}


func (method Method) Parent() Object {
    return Nil
}

func (method Method) Send(raku * Raku, message string, args ... interface {}) (Object, error) {
    return method(raku, message, method, args...) 
}


type Raku struct {
    * Parser
    CommandMap
    Root    * Object
    Nil     * NilClass    
    True    * Boolean
    False   * Boolean

}

func New(peg_filename string) (*Raku, error) {
    var err error
    result := &Raku{}
    result.Parser, err = LoadParser(peg_filename)
    if (err == nil) {
                
    } 
    return result, err
}


func Evaluate(raku * Raku, result Result) {
    _
}



