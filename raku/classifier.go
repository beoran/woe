package raku

/* A classifier classifies Word tokens by their semantic type. */
type Classifier interface {
    /* Classify the given token text. Return the type, and a bool that indicate 
     * whether or not the token was known. */
    Classify(TokenText) (TokenType, bool)
    /* Adds a token type mapping to the classifier. Returns whether or
     * not the addition was acceptable. */
    Add(text TokenText, typ TokenType) bool
}


type DefaultClassifier struct {
    text2type map[TokenText]TokenType; 
}

func (clf DefaultClassifier) Add(text TokenText, typ TokenType) bool {
    if clf.text2type == nil {
        clf.text2type = make(map[TokenText]TokenType)
    }
    clf.text2type[text] = typ
    return true
}

func (clf DefaultClassifier) AddMany(tokenmap map[string] TokenType) bool {
    result := true
    for text, typ := range tokenmap {
        tokentext := TokenText(text);
        call := clf.Add(tokentext, typ)
        result = result && call
    }
    return result
}

func (clf DefaultClassifier) Classify(text TokenText) (TokenType, bool) {
    typ, ok := clf.text2type[text]
    return typ, ok
}

type ClasssifierObject interface {
    Send(message string, arguments ... ClasssifierObject) (ClasssifierObject, error)
}





