# Welcome to the Workers Of Eruta FLOSS project.

## Welcome

We, the maintainer(s) and/or contributor(s), of this FLOSS project, welcome you 
and thank you for your interest in it. This project is brought to you with the 
hope that it will be useful to you, but without any warranty whatsoever. Please 
read the LICENSE  for more details about this.

We welcome contributions from everybody. No matter who you are, 
no matter where you are from, and no matter your opinions on any topic, we
appreciate your contributions to this project as long as they are compatible 
with the project's goals as per GOALS.md and license as per LICENSE. 

## How to play fair

We want to give everyone a fair opportunity to contribute to this project.
To maintain and encourage a fair environment we ask of everyone
involved in the project to try to follow these general guidelines when 
contributing:
 
1. Everyone should try to stay on topic. Please refrain from posting SPAM, 
   trolling, irrelevant discussions, other off topic material.

2. Everyone should try to behave like a reasonable adult. Please refrain 
   from personal attacks, dog piling, bullying or other similar activities.

3. Keep the project goals in mind. Please refrain from demands to change the 
   goals if you are not a maintainer. 

When the project maintainer(s) consider it necessary, any contribution or 
participation in the project that violates these guidelines may be removed or 
edited. At the discretion of the maintainer(s), someone who maliciously or, 
after having been warned repeatedly, keeps on disregarding these guidelines, 
may be temporarily or permanently banned from contributing to the project.

Naturally, these guidelines only apply within the context of the project itself.
It is not our role to judge your behavior that is not directly related to 
the project. The maintainer(s) of this project will may apply project 
related sanctions only for your behavior when contributing. We do this because
would like to welcome anyone, even people with unpopular opinions.

The maintainer(s) of this project will do their best to follow these guidelines 
themselves also, but since they rightfully own the project, they cannot be 
banned from it.

## What to do in case of problems

If you feel that you or your contribution are not being treated fairly, 
if you feel concerned about your personal safety, or if someone is engaged in 
illegal behavior, contact the maintainer(s) of this project at beoran@gmail.com. 
We will do our best to remedy the situation.

## DISCLAIMER

With this text, we hope to welcome you to this project, however, it is merely
a statement of intent to inform you of how we wish to run this project. 
It is in no way legally binding for anyone involved in the project. 
This FLOSS project is run by volunteers, so we do not always have the resources 
to answer you or to help you in case of problems. 

We can only implement this statement of intent on a best effort basis. We cannot 
guarantee that we will be able to uphold the guidleines set out in this text.
Unfortunately, in this day and age it is neccesary to make it amply clear that 
we cannot be held reponsible if something goes awry despite our good intentions. 
Therefore, your participation to this project is subject to the following 
disclaimer:

YOUR PARTICIPATION IN THIS PROJECT IS AT YOUR OWN RISK. ANY EXPRESS OR IMPLIED 
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT, 
ARE DISCLAIMED. IN NO EVENT SHALL THE MAINTAINER(S) AND CONTRIBUTOR(S) OF THIS 
PROJECT BE LIABLE FOR ANY DIRECT, INDIRECT INCIDENTAL, SPECIAL, EXEMPLARY, 
OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, LOSS OF GOODWILL,
LOSS OF REPUTATION, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY 
OF LIABILITY, WHETHER IN  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING 
NEGLIGECE OR OTHERWISE) ARISING IN ANY WAY OUT OF YOUR PARTICIPATION IN THIS
PROJECT, YOUR FAILURE TO PARTICIPATE IN IT, OR YOUR BEING REJECTED FROM 
PARTICIPATION IN IT, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. 

